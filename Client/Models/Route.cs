﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Client.Models
{
    public class Route
    {
        public string route_from { get; set; }
        public string route_to { get; set; }
    }
}