﻿using Client.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Client.Controllers
{
    public class UserController : Controller
    {
        private const String basePayURL = "http://payment-service-uni.apphb.com";
        private RestClient clientPay = new RestClient(basePayURL);
        private string userToken;
        private string userName;

        private const string baseTicketURL = "http://www.mocky.io";
        private RestClient clientTicket = new RestClient(baseTicketURL);

        // GET: User
        public ActionResult Logpage()
        {
            return View();
        }

        //GET:
        public ActionResult GetToken(string loginTextBox, string passTextBox)
        {
            //"http://payment-service-uni.apphb.com/PaymentRest.svc/pay"
            var request = new RestRequest(Method.GET);
            request.Resource = $"{basePayURL}/PaymentRest.svc/pay";
            request.AddParameter("name", loginTextBox, ParameterType.QueryString);
            request.AddParameter("password", passTextBox, ParameterType.QueryString);

            var response = clientPay.Execute<Token>(request);
            userToken = response.Data.token;
            userName = loginTextBox;
           
            return RedirectToAction("TimeToChoose", "User", new { token = userToken });
        }

        public ActionResult TimeToChoose (string token)
        {

            ViewData["response"] = token;
            return View();
        }

        //GET:/GetPossibleRoutes
        public ActionResult RouteList()
        {
            List<Route> routes;
            var request = new RestRequest(Method.GET);
            request.Resource = $"{baseTicketURL}/v2/5b1308e9300000600086113e";
            request.AddParameter("name", userName, ParameterType.QueryString);
            request.AddParameter("token", userToken, ParameterType.QueryString);

            var response = clientTicket.Execute(request);

            //HttpWebRequest webRequest = (HttpWebRequest)WebRequest
            //                               .Create($"{baseTicketURL}/v2/5b108ea52f0000100034f07d");
            //webRequest.AllowAutoRedirect = false;
            //HttpWebResponse resp = (HttpWebResponse)webRequest.GetResponse();

            if ( RestSharpExtensionMethods.IsSuccessful(response))
            {
                routes = JsonConvert.DeserializeObject<List<Route>>(response.Content);
                if (routes.Any() && routes != null)
                {
                    return PartialView(routes);
                } else
                {
                    ViewData["Error"] = "You have problem with your token";
                }
            }
            else
            {
                ViewData["Error"] = "Error " + (int)response.StatusCode;
            }

            return PartialView();
        }

        //GET: /GetRouteDepartureTimes/name token route_from route_to
        [HttpGet]
        public ActionResult ChooseDateTime(string route_from, string route_to)
        {
            //Dictionary<string, List<string>> dictDateTime;
            var request = new RestRequest(Method.GET);
            request.Resource = $"{baseTicketURL}/v2/5b1312fa310000500078be6a";
            request.AddParameter("name", userName, ParameterType.QueryString);
            request.AddParameter("token", userToken, ParameterType.QueryString);
            request.AddParameter("route_from", route_from, ParameterType.QueryString);
            request.AddParameter("route_to", route_to, ParameterType.QueryString);

            var response = clientTicket.Execute(request);

            if (RestSharpExtensionMethods.IsSuccessful(response))
            {
                var dictDateTime = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(response.Content);
                if (dictDateTime.Any() && dictDateTime != null)
                {
                    return View(dictDateTime);
                }
                else
                {
                    ViewData["Error"] = "You have problem with your route";
                }
            }
            else
            {
                ViewData["Error"] = "Error " + (int)response.StatusCode;
            }

            return View();

        }

        public ActionResult ChooseTime(Dictionary<string, List<string>> dict, string date)
        {
            string data = date;
            return PartialView();
        }


    }
}