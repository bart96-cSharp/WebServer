﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Client.Models
{
    public class User
    {
        public string login { get; set; }
        public string password { get; set; }
        public string token { get; set; }
    }
}