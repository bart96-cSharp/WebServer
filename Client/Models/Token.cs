﻿using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Client.Models
{
    public class Token
    {
        [DeserializeAs(Name = "d")]
        public string token { get; set; }
    }
}